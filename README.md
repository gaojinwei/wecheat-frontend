### 相关接口

- 所有主页面请求必须添加 auth 请求头：
  ```
   headers: {
      authorization: "Bearer" + token
    }
  ```
- GET /api/user/:userId/friends 获取好友列表同时返回自身信息，以下简称 fetchFriends

  数据结构：

  ```
  {
      friends:[{avatar, userName, _id}],
      user: {
          avatar, createdAt, updatedAt, userName, _id
      }
  }
  ```

- GET /api/room/:userId 获取消息列表各对话内容，设计有问题需要加用户 id，以下简称 fetchMessages

  数据结构：

  ```
  rooms: [
      {
        created_at,
        updated_at,
        _id,
        users: [
            {
                avatar,
                createdAt,
                friends:[id],
                updatedAt,
                userName,
                _id
            }
        ],
        messages: [
            {message_status, created_at, message_body, user: id, _id}
        ]
      }
  ]
  ```

- POST /api/user/makeNewFriend 同意添加好友请求
  body 参数：
  ```
  {whoseRequest}
  ```
  即同意谁的请求，value 为用户 id

### 流程

1.  目前先 fetchFriends，再 fetchMessages，原因是处理消息来自谁时，只存了用户 id，于是必须在
    Messages.js 中通过 const chatWith = this.props.friends[messageFrom];获取用户详细信息。实际在 messages.js 中 room.messageFrom = usersExceptMe[0].\_id;这一步可以直接存详细信息。暂时懒得改。
