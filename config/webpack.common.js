const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const publicPath = path.resolve(__dirname, "../public");
const distPath = path.resolve(__dirname, "../dist");

module.exports = {
  entry: "./src/index.js",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        loader: "babel-loader"
      },
      {
        test: /\.css$/,
        // use: [
        //   "style-loader",
        //   {
        //     loader: "css-loader",
        //     options: {
        //       modules: true
        //     }
        //   }
        // ]
        use: [
          { loader: "style-loader" },
          {
            loader: "css-loader"
          }
        ]
      }
    ]
  },
  resolve: { extensions: ["*", ".js", ".jsx"] },
  output: {
    //webpack output is served from /dist/
    //path: path.resolve(__dirname, "../dist/"),
    path: path.resolve("dist"),
    //server-relative
    // publicPath: "/dist/",
    //并不会改变js生成到dist目录的路径，而是改变了index.html引入js的路径
    publicPath: "/",
    filename: "index.[contenthash].js"
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin([
      {
        //需要指定绝对路径，否则按照context路径
        from: `${publicPath}/static`,
        to: `${distPath}/static`
      },
      {
        from: `${publicPath}/favicon`,
        to: `${distPath}/favicon`
      }
    ]),
    //使用html-webpack-plugin生成html，并自动引入bundle，避免手动设置的相对路径失效
    new HtmlWebpackPlugin({
      //后面的路径以当前路径为准，即webpack.common.js
      template: path.resolve(__dirname, "../public/index.html"),
      filename: "index.html"
    })
  ]
};
