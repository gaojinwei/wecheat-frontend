const path = require("path");
const webpack = require("webpack");

const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
  //运行development server时不用再添加mode flag
  //从webpack v4开始，mode自动配置了DefinePlugin，key: process.env.NODE_ENV，value: 指定的mode
  mode: "production"
});
