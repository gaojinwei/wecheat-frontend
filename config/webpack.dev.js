const path = require("path");
const webpack = require("webpack");

const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
  //运行development server时不用再添加mode flag
  //从webpack v4开始，mode自动配置了DefinePlugin，key: process.env.NODE_ENV，value: 指定的mode
  mode: "development",
  //mode: "production",
  devtool: "inline-source-map",
  devServer: {
    //到哪找index.html以及
    //Content not from webpack is served from /Users/gaojinwei/wecheat-frontend/public/
    //则一旦使用html-webpack-plugin在dist下生成index.html并以此为入口，必须将静态文件拷贝至相同目录
    //contentBase: path.join(__dirname, "../public/"),
    contentBase: path.join(__dirname, "../dist/"),
    //不设置直接ip访问不了
    host: "0.0.0.0",
    //contentBase: path.join(__dirname, "dist/"),
    port: 3888,
    //不指定会假设bundle files在根目录（待验证）
    //publicPath: "http://localhost:3888/dist/",
    //hotOnly: true,
    writeToDisk: true,
    //使用HTML5 history API 而不是URL的hash portion
    historyApiFallback: true
  }
  // plugins: [new webpack.HotModuleReplacementPlugin()]
});
