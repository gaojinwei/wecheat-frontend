import { SAVE_FRIENDS } from "../actions/constants/action-types";

//不再是原数组形式，id为key，其他属性为value
export default function friends(state = [], action) {
  switch (action.type) {
    case SAVE_FRIENDS:
      const friends = {};
      action.payload.forEach(friend => {
        //其余属性组成rest对象
        const { _id, ...rest } = friend;
        friends[_id] = rest;
      });
      return friends;
    default:
      return state;
  }
}
