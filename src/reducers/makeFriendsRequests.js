import {
  APPEND_FRIEND_REQUESTS,
  DELETE_HANDLED_REQUEST
} from "../actions/constants/action-types";

export default function makeFriendsRequest(state = [], action) {
  switch (action.type) {
    case APPEND_FRIEND_REQUESTS:
      const { from, userName, avatar, message } = action.payload;
      const request = {};
      request[from] = action.payload;
      return [...state, request];
    case DELETE_HANDLED_REQUEST:
      const whoseRequest = action.payload;
      const newState = state.filter(item => item[from].from !== whoseRequest);
      return newState;
    default:
      return state;
  }
}
