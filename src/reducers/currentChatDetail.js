import { SAVE_CURRENT_CHAT_DETAIL } from "../actions/constants/action-types";

export default function currentChatDetail(
  state = { chatWith: {}, messages: [] },
  action
) {
  switch (action.type) {
    case SAVE_CURRENT_CHAT_DETAIL:
      return action.payload;
    default:
      return state;
  }
}
