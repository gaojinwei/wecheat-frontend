import { SET_MESSAGE_TYPING } from "../actions/constants/action-types";

export default function messages(state = "", action) {
  switch (action.type) {
    case SET_MESSAGE_TYPING:
      return action.payload;
    default:
      return state;
  }
}
