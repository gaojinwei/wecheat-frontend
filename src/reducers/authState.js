import {
  SET_AUTH_STATUS,
  SET_JWT_TOKEN,
  SAVE_USER_ID
} from "../actions/constants/action-types";

const isAuthenticatedItem = window.localStorage.getItem("isAuthenticated");
export default function authState(
  state = {
    isAuthenticated: isAuthenticatedItem === "true",
    token: window.localStorage.getItem("token") || "",
    id: window.localStorage.getItem("id") || ""
  },
  action
) {
  switch (action.type) {
    case SET_AUTH_STATUS:
      window.localStorage.setItem("isAuthenticated", action.payload);
      return { ...state, isAuthenticated: action.payload };
    case SET_JWT_TOKEN:
      window.localStorage.setItem("token", action.payload);
      return { ...state, token: action.payload };
    case SAVE_USER_ID:
      window.localStorage.setItem("id", action.payload);

      return { ...state, id: action.payload };
    default:
      return state;
  }
}
