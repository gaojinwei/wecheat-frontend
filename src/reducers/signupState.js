import {
  SET_SIGNUP_USERNAME,
  EMPTY_SIGNUP_USERNAME,
  SET_SIGNUP_PASSWORD,
  EMPTY_SIGNUP_PASSWORD,
  SET_SIGNUP_REPEAT_PASSWORD,
  EMPTY_SIGNUP_REPEAT_PASSWORD
} from "../actions/constants/action-types";

export default function signupState(
  state = { username: "", password: "", repeatedPassword: "" },
  action
) {
  switch (action.type) {
    case SET_SIGNUP_USERNAME:
      return { ...state, username: action.payload };
    case EMPTY_SIGNUP_USERNAME:
      return { ...state, username: "" };
    case SET_SIGNUP_PASSWORD:
      return { ...state, password: action.payload };
    case EMPTY_SIGNUP_PASSWORD:
      return { ...state, password: "" };
    case SET_SIGNUP_REPEAT_PASSWORD:
      return { ...state, repeatedPassword: action.payload };
    case EMPTY_SIGNUP_REPEAT_PASSWORD:
      return { ...state, repeatedPassword: "" };
    default:
      return state;
  }
}
