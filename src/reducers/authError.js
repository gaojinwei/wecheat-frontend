import {
  SET_AUTH_ERROR,
  EMPTY_AUTH_ERROR
} from "../actions/constants/action-types";

export default function authError(state = "", action) {
  switch (action.type) {
    case SET_AUTH_ERROR:
      return action.payload;
    case EMPTY_AUTH_ERROR:
      return "";
    default:
      return state;
  }
}
