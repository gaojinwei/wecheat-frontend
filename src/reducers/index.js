import { combineReducers } from "redux";
import usernameTyping from "./usernameTyping";
import passwordTyping from "./passwordTyping";
import signupState from "./signupState";
import authError from "./authError";
import authState from "./authState";
import friends from "./friends";
import messages from "./messages";
import myInfo from "./myInfo";
import currentChatDetail from "./currentChatDetail";
import messageTyping from "./messageTyping";
import searchedUser from "./searchedUser";
import makeFriendsRequests from "./makeFriendsRequests";

//每个key都是一个state
export default combineReducers({
  usernameTyping,
  passwordTyping,
  signupState,
  authError,
  authState,
  friends,
  messages,
  myInfo,
  currentChatDetail,
  messageTyping,
  searchedUser,
  makeFriendsRequests
});

// export default state => {
//   return state;
// };
