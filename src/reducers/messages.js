import { SAVE_MESSAGES } from "../actions/constants/action-types";

// [{
//   messageFrom:userId, messages:[], updated_at, id:roomId
// }]
export default function messages(state = [], action) {
  switch (action.type) {
    case SAVE_MESSAGES:
      return action.payload;
    default:
      return state;
  }
}
