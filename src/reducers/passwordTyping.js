import {
  SET_PASSWORD_VALUE,
  EMPTY_PASSWORD_VALUE
} from "../actions/constants/action-types";

export default function passwordTyping(state = "", action) {
  switch (action.type) {
    case SET_PASSWORD_VALUE:
      return action.payload;
    case EMPTY_PASSWORD_VALUE:
      return "";
    default:
      return state;
  }
}
