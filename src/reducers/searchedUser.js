import { SAVE_SEARCHED_USER } from "../actions/constants/action-types";

export default function searchedUser(
  state = { avatar: "", userName: "", _id: "" },
  action
) {
  switch (action.type) {
    case SAVE_SEARCHED_USER:
      return action.payload;

    default:
      return state;
  }
}
