import {
  SET_USERNAME_VALUE,
  EMPTY_USERNAME_VALUE
} from "../actions/constants/action-types";

export default function usernameTyping(state = "", action) {
  switch (action.type) {
    case SET_USERNAME_VALUE:
      return action.payload;
    case EMPTY_USERNAME_VALUE:
      return "";
    default:
      return state;
  }
}
