import { SAVE_MY_INFO } from "../actions/constants/action-types";

export default function myInfo(state = { userName: "", avatar: "" }, action) {
  switch (action.type) {
    case SAVE_MY_INFO:
      return action.payload;
    default:
      return state;
  }
}
