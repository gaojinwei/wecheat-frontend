import React, { Component } from "react";

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  withRouter
} from "react-router-dom";

import { TransitionGroup, CSSTransition } from "react-transition-group";

import Login from "./containers/Login";
import Signup from "./containers/Signup";
import Protected from "./containers/Protected";
import PrivateRoute from "./containers/PrivateRoute";
import FakeHome from "./containers/FakeHome";
import ChatDetail from "./containers/ChatDetail";
import UserProfile from "./containers/UserProfile";
import SearchUser from "./containers/SearchUser";
import SendFriendshipRequest from "./containers/SendFriendshipRequest";
//import "./Routes.css";
import "./App.css";

function Routes({ location }) {
  //console.log(location.key);
  return (
    <React.Fragment>
      <TransitionGroup className="transition-group">
        <CSSTransition
          in={true}
          key={location.key}
          //timeout={{ enter: 60000, exit: 30000 }}
          timeout={900}
          classNames="fade"
        >
          {/* CSSTransition只能有一个Route，要用Switch */}
          <section className="route-section">
            {/*  必须加location={location}否则同时出现相同page*/}
            <Switch location={location}>
              <Route exact path="/" component={FakeHome} />
              <Route exact path="/login" component={Login} />
              <Route path="/signup" component={Signup} />
            </Switch>
          </section>
        </CSSTransition>
      </TransitionGroup>
      <PrivateRoute path="/protected" component={Protected} />
      <PrivateRoute path="/private/chatDetail/:id" component={ChatDetail} />
      <PrivateRoute
        path="/private/UserProfile/:userId"
        component={UserProfile}
      />
      <PrivateRoute path="/private/searchUser" component={SearchUser} />
      <PrivateRoute
        path="/private/sendFriendshipRequest"
        component={SendFriendshipRequest}
      />
    </React.Fragment>
  );
}

export default withRouter(Routes);
