import React, { Component } from "react";
import { connect } from "react-redux";

import "./Chat.css";

const Chat = ({ message, chatWithAvatar, myAvatar }) => {
  const { message_body, is_my_msg } = message;
  return (
    <div className={`Chat_wrapper ${is_my_msg ? "is_my_msg" : ""}`}>
      {!is_my_msg && <img src={chatWithAvatar} className="Chat_avatar" />}
      <span className={`chat ${is_my_msg ? "is_my_msg" : ""}`}>
        {message_body}
      </span>
      {is_my_msg && <img src={myAvatar} className="Chat_avatar is_my_msg" />}
    </div>
  );
};

function mapStateToProps(state) {
  const { avatar } = state.myInfo;
  return {
    myAvatar: avatar
  };
}

export default connect(mapStateToProps)(Chat);
