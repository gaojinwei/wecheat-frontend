import React, { Component } from "react";

import "./Room.css";

function Room(props) {
  const { chatWith, updatedAt, lastMessage, enterRoom } = props;
  const { userName, avatar } = chatWith;
  return (
    <div onClick={enterRoom} className="room_container">
      <img src={avatar} className="room_avatar" />
      <div className="room_content">
        <div className="room_contact">{userName}</div>
        <div className="room_message">{lastMessage}</div>
        <span className="room_date">{updatedAt}</span>
      </div>
    </div>
  );
}

export default Room;
