import React, { Component } from "react";

import "./GeneralMain.css";

function GeneralMain({ children }) {
  return <main className="GeneralMain">{children}</main>;
}

export default GeneralMain;
