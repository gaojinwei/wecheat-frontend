import React from "react";

function CrossShape({ onClick, black }) {
  return (
    <svg
      onClick={onClick}
      width="20"
      height="20"
      viewBox="0 0 6.82666 6.82666"
      style={
        black
          ? { position: "fixed", top: "20px", left: "20px" }
          : { position: "absolute", right: "0px" }
      }
    >
      {/* <defs>
        <style type="text/css">
           .fil1 {fill:none}
           .fil0 {fill:#a7a6a6;fill-rule:nonzero}
        </style>
      </defs> */}
      <g id="Layer_x0020_1">
        <path
          fill={black ? "rgba(0,0,0,1)" : "#a7a6a6"}
          fillRule="nonzero"
          d="M5.91083 1.2175c0.0833031,-0.0833031 0.0833031,-0.218366 0,-0.301669 -0.0833031,-0.0833031 -0.218366,-0.0833031 -0.301669,0l-4.69334 4.69333c-0.0833031,0.0833031 -0.0833031,0.218366 0,0.301669 0.0833031,0.0833031 0.218366,0.0833031 0.301669,0l4.69334 -4.69333z"
        />
        <path
          fill={black ? "rgba(0,0,0,1)" : "#a7a6a6"}
          fillRule="nonzero"
          d="M1.2175 0.915827c-0.0833031,-0.0833031 -0.218366,-0.0833031 -0.301669,0 -0.0833031,0.0833031 -0.0833031,0.218366 0,0.301669l4.69334 4.69333c0.0833031,0.0833031 0.218366,0.0833031 0.301669,0 0.0833031,-0.0833031 0.0833031,-0.218366 0,-0.301669l-4.69334 -4.69333z"
        />
      </g>
      <rect height="6.82666" width="6.82666" fill="none" />
    </svg>
  );
}

export default CrossShape;
