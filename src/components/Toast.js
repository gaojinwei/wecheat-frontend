import React, { Component } from "react";

import "./Toast.css";

var ref;

class Toast extends Component {
  constructor(props) {
    super(props);
    ref = this;
    this.state = {
      message: "",
      show: false
    };
  }
  render() {
    const { message, show } = this.state;
    return show ? <div className="toast">{message}</div> : null;
  }
}

export const showToast = err => {
  if (ref.state.show) {
    return;
  }
  ref.setState(
    {
      message: err,
      show: true
    },
    () => {
      setTimeout(
        () =>
          ref.setState({
            message: "",
            show: false
          }),
        6500
      );
    }
  );
};

export default Toast;
