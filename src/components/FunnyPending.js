import React from "react";
import classNames from "classnames";

import "./FunnyPending.css";

function animate({ timing, draw, duration }) {
  let start = performance.now();

  //console.log(this);
  FunnyPending.requestId = requestAnimationFrame(function animate(time) {
    // timeFraction goes from 0 to 1
    let timeFraction = (time - start) / duration;
    if (timeFraction > 1) timeFraction = 1;

    // calculate the current animation state
    let progress = timing(timeFraction);

    draw(progress); // draw it

    if (timeFraction < 1) {
      FunnyPending.requestId = requestAnimationFrame(animate);
    }
  });
}

function linear(timeFraction) {
  return timeFraction;
}

class FunnyPending extends React.Component {
  constructor(props) {
    super(props);

    this.imgRef = React.createRef();
  }

  render() {
    return (
      <div className="FunnyPending_wrapper">
        <img
          ref={this.imgRef}
          src="/static/huaji.jpeg"
          className="FunnyPending_huaji"
        />
      </div>
    );
  }

  componentDidMount() {
    //const { pending } = this.props;
    const imgNode = this.imgRef.current;
    //if (pending) {
    function draw(progress) {
      imgNode.style.transform = `rotate(${360 * progress}deg)`;
    }

    animate({
      timing: linear,
      draw,
      duration: 10000
    });
    // }
  }

  //初始不调用，props改变会调用一次
  componentDidUpdate() {
    //确保componentDidUpdate只调用一次
    this.updateTimes = this.updateTimes ? ++this.updateTimes : 1;
    if (this.updateTimes === 1) {
      cancelAnimationFrame(FunnyPending.requestId);
      const imgNode = this.imgRef.current;
      function draw(progress) {
        imgNode.style.transform = `rotate(${360 * progress}deg)`;
        imgNode.style.width = `${50 + 100 * progress}px`;
      }

      animate({
        timing: linear,
        draw,
        duration: 1000
      });
    }
  }
}

// function FunnyPending(props) {
//   const { pending } = props;
//   const rotateClass = classNames({
//     FunnyPending_huaji: true,
//     rotateSlowly: pending,
//     rotateQuckily: !pending
//   });
//   return (
//     <div className="FunnyPending_wrapper">
//       <img src="/static/huaji.jpeg" className={rotateClass} />
//     </div>
//   );
// }

export default FunnyPending;
