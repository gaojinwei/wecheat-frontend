import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { withRouter } from "react-router";

import "./GeneralHead.css";

function GeneralHead(props) {
  const { title } = props;
  return (
    <header className="GeneralHead">
      <div
        style={{ padding: "5px 10px" }}
        onClick={() => props.history.goBack()}
      >
        <FontAwesomeIcon
          icon="angle-left"
          size="2x"
          style={{ marginRight: "5px" }}
        />
      </div>
      {title && <span className="GeneralHead_title">{title}</span>}
    </header>
  );
}

export default withRouter(GeneralHead);
