import React, { Component } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
  fetchCurrentChatDetail,
  setMessageTyping,
  postMessage
} from "../actions/messages";

import Chat from "../components/Chat";

import "./ChatDetail.css";

class ChatDetail extends Component {
  constructor(props) {
    super(props);
    this.chatsRef = React.createRef();
  }

  render() {
    const { chatWith, messages, myAvatar } = this.props;
    const chats = messages.map((message, index) => {
      return (
        <Chat
          key={message.id}
          message={message}
          chatWithAvatar={chatWith.avatar}
          myAvatar={myAvatar}
        />
      );
    });
    return (
      <React.Fragment>
        <header className="chatDetail_header">
          <div
            style={{ display: "flex", alignItems: "center" }}
            onClick={() => this.props.history.goBack()}
          >
            <FontAwesomeIcon
              icon="angle-left"
              size="2x"
              style={{ marginRight: "5px" }}
            />
            {chatWith.userName}
          </div>
        </header>
        <div style={{ padding: "50px 0px 72px 10px", background: "#eaeaea" }}>
          <main className="chatDetail_main" ref={this.chatsRef}>
            {chats}
          </main>
        </div>
        <footer className="chatDetail_footer">
          <form className="chatDetail_form" onSubmit={this.sendMessage}>
            <input
              className="chatDetail_input"
              onChange={this.typingMessage}
              value={this.props.messageTyping}
            />
            <button className="chatDetail_button">发送</button>
          </form>
        </footer>
      </React.Fragment>
    );
  }

  componentDidMount() {
    const { match, fetchCurrentChatDetail } = this.props;
    const roomId = match.params.id;
    fetchCurrentChatDetail(roomId);
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  typingMessage = e => {
    const message = e.target.value;
    this.props.setMessageTyping(message);
  };

  scrollToBottom = () => {
    this.chatsRef.current.scrollTop = this.chatsRef.current.scrollHeight;
  };

  sendMessage = e => {
    e.preventDefault();
    const { match, messageTyping, postMessage } = this.props;
    const roomId = match.params.id;
    postMessage(roomId, messageTyping);
  };
}

function mapStateToProps(state) {
  const { chatWith, messages } = state.currentChatDetail;
  const messageTyping = state.messageTyping;
  const { userName, avatar } = state.myInfo;
  return {
    chatWith,
    messages,
    messageTyping,
    myAvatar: avatar
  };
}

export default connect(
  mapStateToProps,
  { fetchCurrentChatDetail, setMessageTyping, postMessage }
)(ChatDetail);
