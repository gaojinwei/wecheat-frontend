import React, { Component } from "react";
import { connect } from "react-redux";
import classNames from "classnames";
import { Link, withRouter } from "react-router-dom";
import axios from "axios";

import CrossShape from "../components/svgs/CrossShape";
import {
  setUsernameValue,
  emptyUsernameValue,
  setPasswordValue,
  emptyPasswordValue,
  setAuthError,
  emptyAuthError,
  setAuthStatus,
  setJwtToken,
  saveUserId
} from "../actions/auth";
import Modal from "./Modal";
import Dialog from "./Dialog";

import varConfig from "../varConfig";

import "./Login.css";

class Login extends Component {
  render() {
    const { usernameTyping, passwordTyping, authError } = this.props;
    const btnClass = classNames({
      authButton: true,
      forbidden: !usernameTyping || !passwordTyping
    });

    return (
      <React.Fragment>
        {authError && (
          <Modal>
            <Dialog
              title="登录失败"
              confirm={() => this.dispatchEmptyAuthError()}
            />
          </Modal>
        )}
        <CrossShape black={true} onClick={this.props.history.goBack} />
        <form className="container" onSubmit={this.login}>
          <div className="inputItem">
            昵称
            <input
              className="inputNode"
              placeholder="请填写昵称"
              onChange={this.dispatchUsernameTyping}
              value={usernameTyping}
              autoComplete="username"
            />
            {usernameTyping && (
              <CrossShape onClick={this.dispatchEmptyUsername} />
            )}
          </div>
          <div className="inputItem">
            密码
            <input
              className="inputNode"
              type="password"
              placeholder="请填写密码"
              onChange={this.dispatchPasswordTyping}
              value={passwordTyping}
              autoComplete="current-password"
            />
            {passwordTyping && (
              <CrossShape onClick={this.dispatchEmptyPassword} />
            )}
          </div>
          <button className={btnClass}>登录</button>
        </form>
        <Link to="/signup" className="registerLink">
          注册
        </Link>
      </React.Fragment>
    );
  }

  dispatchUsernameTyping = e => {
    const username = e.target.value;
    this.props.setUsernameValue(username);
  };

  dispatchEmptyUsername = () => this.props.emptyUsernameValue();

  dispatchPasswordTyping = e => {
    const password = e.target.value;
    this.props.setPasswordValue(password);
  };

  dispatchEmptyPassword = () => this.props.emptyPasswordValue();

  dispatchSetAuthError = error => this.props.setAuthError(error);
  dispatchEmptyAuthError = () => this.props.emptyAuthError();

  login = async e => {
    e.preventDefault();
    const { usernameTyping, passwordTyping, history } = this.props;

    const res = await axios.post(`${varConfig.backendOrigin}/signin`, {
      userName: usernameTyping,
      password: passwordTyping
    });
    const { data } = res;
    if (data) {
      const { token, id } = data;
      this.props.setJwtToken(token);
      this.props.saveUserId(id);
      this.props.setAuthStatus(true);

      history.push("/protected");
    }
  };
}

function mapStateToProps(state) {
  return {
    usernameTyping: state.usernameTyping,
    passwordTyping: state.passwordTyping,
    authError: state.authError
  };
}

export default connect(
  mapStateToProps,
  {
    setUsernameValue,
    emptyUsernameValue,
    emptyPasswordValue,
    setPasswordValue,
    setAuthError,
    emptyAuthError,
    setAuthStatus,
    setJwtToken,
    saveUserId
  }
)(withRouter(Login));
