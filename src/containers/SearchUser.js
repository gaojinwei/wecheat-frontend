import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";

import { saveSearchedUser } from "../actions/friends";

import GeneralHead from "../components/GeneralHead";
import GeneralMain from "../components/GeneralMain";

import varConfig from "../varConfig";

import "./SearchUser.css";

class SearchUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTyping: "",
      userExist: true
    };
  }

  render() {
    return (
      <React.Fragment>
        <GeneralHead />
        <GeneralMain>
          <form className="SearchUser_form" onSubmit={this.search}>
            <input
              className="SearchUser_input"
              placeholder="昵称"
              onChange={this.typing}
            />
            <button className="SearchUser_button">搜索</button>
          </form>
          {!this.state.userExist && (
            <div style={{ color: "rgba(190, 189, 193, 1)" }}>该用户不存在</div>
          )}
        </GeneralMain>
      </React.Fragment>
    );
  }

  typing = e => {
    const typing = e.target.value;
    this.setState({
      searchTyping: typing
    });
  };

  search = e => {
    e.preventDefault();
    const { token } = this.props;
    const { searchTyping } = this.state;
    axios
      .get(
        `${varConfig.backendOrigin}/api/user/single?userName=${searchTyping}`,
        {
          headers: {
            authorization: "Bearer" + token
          }
        }
      )
      .then(res => {
        this.setState({
          userExist: true
        });
        this.props.saveSearchedUser(res.data.user);
        const userId = res.data.user._id;
        this.props.history.push(`/private/userProfile/${userId}`);
      })
      .catch(error => {
        this.setState({
          userExist: false
        });
        console.log(error);
      });
  };
}

function mapStateToProps(state) {
  const { token, id } = state.authState;
  return {
    token
  };
}

export default connect(
  mapStateToProps,
  { saveSearchedUser }
)(SearchUser);
