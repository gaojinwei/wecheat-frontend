import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
//import { HashRouter as Router, Route, Link } from "react-router-dom";

import "./Home.css";

class FakeHome extends Component {
  render() {
    // return (
    //   <React.Fragment>
    //     <FontAwesomeIcon icon="user" color="green" />
    //     <FontAwesomeIcon icon="user-friends" color="green" />
    //     <FontAwesomeIcon icon="comment" color="green" />
    //     <FontAwesomeIcon icon="compass" color="green" />
    //     <FontAwesomeIcon icon="poo" color="green" />
    //     <FontAwesomeIcon icon="plus-circle" color="green" />
    //     <FontAwesomeIcon icon="user-plus" color="green" />
    //     <FontAwesomeIcon icon="trash-alt" color="green" />
    //     <Link to="/login">login</Link>
    //     <Link to="/protected">protected</Link>
    //   </React.Fragment>
    // );

    return (
      <div className="home_wrapper">
        <div className="home_colorPlaceholder" />
        <img className="home_planet" src="/static/saturn.jpg" />
        <ul className="home_buttons">
          <li>
            <Link to="/login">
              <button className="loginButton">登录</button>
            </Link>
          </li>
          <li>
            <Link to="/signup">
              <button className="signupButton">注册</button>
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default FakeHome;
