import React, { Component } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  BrowserRouter as Router,
  Route,
  NavLink,
  Switch
} from "react-router-dom";
import io from "socket.io-client";

import { fetchFriends, appendFriendRequest } from "../actions/friends";

import Messages from "./Messages";
import Friends from "./Friends";
import Find from "./Find";
import Me from "./Me";

import varConfig from "../varConfig";

import "./Protected.css";

class Protected extends Component {
  render() {
    return (
      //别再用Router包装，否则回不到外部Router
      // <Router>
      <React.Fragment>
        <Switch>
          <Route exact path="/protected" component={Messages} />
          <Route exact path="/protected/friends" component={Friends} />
          <Route exact path="/protected/find" component={Find} />
          <Route exact path="/protected/me" component={Me} />
        </Switch>
        <ul className="protected_list_wrapper">
          <li>
            <NavLink
              to="/protected"
              className="protected_list"
              activeStyle={{
                color: "rgb(51, 136, 255)"
              }}
              exact
            >
              <FontAwesomeIcon
                icon="comment"
                size="2x"
                className="protecte_bottomIcon"
              />
              消息
            </NavLink>
          </li>
          <li className="protected_list">
            <NavLink
              to="/protected/friends"
              className="protected_list"
              activeStyle={{
                color: "rgb(51, 136, 255)"
              }}
            >
              <FontAwesomeIcon
                icon="user-friends"
                size="2x"
                className="protecte_bottomIcon"
              />
              通讯录
            </NavLink>
          </li>
          <li className="protected_list">
            <NavLink
              to="/protected/find"
              className="protected_list"
              activeStyle={{
                color: "rgb(51, 136, 255)"
              }}
            >
              <FontAwesomeIcon
                icon="poo"
                size="2x"
                className="protecte_bottomIcon"
              />
              发现
            </NavLink>
          </li>
          <li className="protected_list">
            <NavLink
              to="/protected/me"
              className="protected_list"
              activeStyle={{
                color: "rgb(51, 136, 255)"
              }}
            >
              <FontAwesomeIcon
                icon="user"
                size="2x"
                className="protecte_bottomIcon"
              />
              我
            </NavLink>
          </li>
        </ul>
      </React.Fragment>
      // {/* </Router> */}
    );
  }

  componentDidMount() {
    this.props.fetchFriends();
    const socket = io(varConfig.backendOrigin);
    window.identifySocket = socket;
    socket.emit("register", this.props.userId);
    socket.on("addFriendRequest", serverData => {
      const { userDetailInfo, message, from } = serverData;
      const { avatar, userName } = userDetailInfo;
      this.props.appendFriendRequest({ from, message, avatar, userName });
    });
    socket.on("newFriendNotify", serverData => {
      this.props.fetchFriends();
    });
    // socket.on("connect", function() {
    //   console.log("client connect");
    // });
  }
}

function mapStateToProps(state) {
  const { friends, authState } = state;
  return {
    friends,
    userId: authState.id
  };
}

export default connect(
  mapStateToProps,
  { fetchFriends, appendFriendRequest }
)(Protected);
