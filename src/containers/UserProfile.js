import React, { Component } from "react";
import { connect } from "react-redux";

import { saveCurrentChatDetail } from "../actions/messages";

import GeneralHead from "../components/GeneralHead";
import GeneralMain from "../components/GeneralMain";

import "./UserProfile.css";

class UserProfile extends Component {
  render() {
    const { match, friends, messages } = this.props;
    const userId = match.params.userId;
    const user = friends[userId];
    const isStranger = user ? false : true;
    const { userName, avatar } = user || this.props.searchedUser;

    const whichRoom = messages.filter(room => room.messageFrom === userId);
    const { id, messages: currentRoomMessages } = whichRoom[0] || {
      id: "",
      messages: []
    };
    return (
      <React.Fragment>
        <GeneralHead />
        {this.props.myName === userName ? (
          <GeneralMain>搜你自己干嘛</GeneralMain>
        ) : (
          <GeneralMain>
            <div className="UserProfile_info">
              <img src={avatar} className="UserProfile_avatar" />
              <span>{userName}</span>
            </div>
            <button
              className="UserProfile_button"
              onClick={() => {
                if (isStranger) {
                  this.navigateToSendFriendshipRequest();
                  return;
                }
                this.navigateToChatDetail(id, user, currentRoomMessages);
              }}
            >
              {isStranger ? "添加到通讯录" : "发消息"}
            </button>
          </GeneralMain>
        )}
      </React.Fragment>
    );
  }

  navigateToChatDetail = (roomId, chatWith, messages) => {
    this.props.saveCurrentChatDetail({
      chatWith,
      messages
    });
    this.props.history.push(`/private/chatDetail/${roomId}`);
  };

  navigateToSendFriendshipRequest = () => {
    this.props.history.push(`/private/sendFriendshipRequest`);
  };

  componentDidMount() {}
}

function mapStateToProps(state) {
  const { messages, friends, myInfo, searchedUser } = state;
  return {
    myName: myInfo.userName,
    messages,
    friends,
    searchedUser
  };
}

export default connect(
  mapStateToProps,
  { saveCurrentChatDetail }
)(UserProfile);
