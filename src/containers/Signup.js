import React, { Component } from "react";
import { connect } from "react-redux";
import classNames from "classnames";
import { withRouter } from "react-router-dom";
import axios from "axios";

import CrossShape from "../components/svgs/CrossShape";
import {
  setSignupUsername,
  emptySignupUsername,
  setSignupPassword,
  emptySignupPassword,
  setSignupRepeatPassword,
  emptySignupRepeatPassword,
  setAuthError,
  emptyAuthError,
  setAuthStatus,
  setJwtToken,
  saveUserId
} from "../actions/auth";

import Modal from "./Modal";
import Dialog from "./Dialog";
import FunnyPending from "../components/FunnyPending";

import varConfig from "../varConfig";

import "./Login.css";
import "./Signup.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pending: false,
      showFunnyPending: false
    };
  }
  render() {
    const { pending, showFunnyPending } = this.state;
    const {
      usernameTyping,
      passwordTyping,
      repeatedPasswordTyping,
      authError
    } = this.props;
    const btnClass = classNames({
      authButton: true,
      forbidden: !usernameTyping || !passwordTyping || !repeatedPasswordTyping
    });

    return (
      <React.Fragment>
        {authError && (
          <Modal>
            <Dialog
              title="注册失败"
              confirm={() => this.dispatchEmptyAuthError()}
              errorMessage={authError}
            />
          </Modal>
        )}
        {showFunnyPending && (
          <Modal>
            <FunnyPending pending={pending} />
          </Modal>
        )}
        <CrossShape black={true} onClick={this.props.history.goBack} />
        <form className="container" onSubmit={this.signup}>
          <div className="inputItem">
            <span className="inputName">昵称</span>
            <input
              className="inputNode--marginLeft20"
              placeholder="请填写昵称"
              onChange={this.dispatchUsernameTyping}
              value={usernameTyping}
              autoComplete="username"
            />
            {usernameTyping && (
              <CrossShape onClick={this.dispatchEmptyUsername} />
            )}
          </div>
          <div className="inputItem">
            <span className="inputName">密码</span>
            <input
              type="password"
              className="inputNode--marginLeft20"
              placeholder="请填写密码"
              onChange={this.dispatchPasswordTyping}
              value={passwordTyping}
              autoComplete="new-password"
            />
            {passwordTyping && (
              <CrossShape onClick={this.dispatchEmptyPassword} />
            )}
          </div>
          <div className="inputItem">
            <span className="inputName">确认密码</span>
            <input
              type="password"
              className="inputNode--marginLeft20"
              placeholder="请填写密码"
              onChange={this.dispatchRepeatedPasswordTyping}
              value={repeatedPasswordTyping}
              autoComplete="new-password"
            />
            {repeatedPasswordTyping && (
              <CrossShape onClick={this.dispatchEmptyRepeatedPassword} />
            )}
          </div>
          <button className={btnClass}>注册</button>
        </form>
      </React.Fragment>
    );
  }

  dispatchUsernameTyping = e => {
    const username = e.target.value;
    this.props.setSignupUsername(username.trim());
  };

  dispatchEmptyUsername = () => this.props.emptySignupUsername();

  dispatchPasswordTyping = e => {
    const password = e.target.value;
    this.props.setSignupPassword(password.trim());
  };

  dispatchEmptyPassword = () => this.props.emptySignupPassword();

  dispatchRepeatedPasswordTyping = e => {
    const password = e.target.value;
    this.props.setSignupRepeatPassword(password.trim());
  };

  dispatchEmptyRepeatedPassword = () => this.props.emptySignupRepeatPassword();

  dispatchSetAuthError = error => this.props.setAuthError(error);
  dispatchEmptyAuthError = () => this.props.emptyAuthError();

  dispatchSetAuthStatus = () => {
    this.props.setAuthStatus(true);
  };

  dipatchSetJwtToken = token => {
    this.props.setJwtToken(token);
  };

  signup = async e => {
    e.preventDefault();

    const {
      usernameTyping,
      passwordTyping,
      repeatedPasswordTyping,
      history
    } = this.props;

    if (!usernameTyping || !passwordTyping || !repeatedPasswordTyping) {
      return;
    } else if (usernameTyping.length > 10) {
      this.dispatchSetAuthError("昵称长度不能超过十位");
    } else if (passwordTyping.length < 6 || passwordTyping.length > 20) {
      this.dispatchSetAuthError("密码长度不能小于六位或大于二十位");
    } else if (passwordTyping !== repeatedPasswordTyping) {
      this.dispatchSetAuthError("两次密码输入不一致");
    } else {
      this.setState({
        pending: true,
        showFunnyPending: true
      });
      const res = await axios.post(`${varConfig.backendOrigin}/signup`, {
        userName: usernameTyping,
        password: passwordTyping
      });
      const { data } = res;
      if (data) {
        const { token, id } = data;
        this.dipatchSetJwtToken(token);
        this.props.saveUserId(id);
        this.dispatchSetAuthStatus();
        //不再慢转
        this.setState({
          pending: false
        });

        //1s快转后移除
        setTimeout(() => {
          //当前页面组件会re-render,从而子组件re-render
          history.push("/protected");
        }, 1000);
      }
    }
  };
}

function mapStateToProps(state) {
  const { username, password, repeatedPassword } = state.signupState;
  const { authError } = state;
  return {
    usernameTyping: username,
    passwordTyping: password,
    repeatedPasswordTyping: repeatedPassword,
    authError
  };
}

export default connect(
  mapStateToProps,
  {
    setSignupUsername,
    emptySignupUsername,
    setSignupPassword,
    emptySignupPassword,
    setSignupRepeatPassword,
    emptySignupRepeatPassword,
    setAuthError,
    emptyAuthError,
    setAuthStatus,
    setJwtToken,
    saveUserId
  }
)(withRouter(Login));
