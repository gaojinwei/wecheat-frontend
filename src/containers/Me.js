import React, { Component } from "react";
import { connect } from "react-redux";

import Toast, { showToast } from "../components/Toast";
import varConfig from "../varConfig";

import { saveMyInfo } from "../actions/myInfo";

import "./Me.css";

class Me extends Component {
  render() {
    const { userName, avatar } = this.props;
    return (
      <React.Fragment>
        <div style={{ padding: "0 10px" }}>
          <label htmlFor="uploader">
            <div className="me_item me_avatarArea">
              <span>头像</span>
              <img src={avatar} />
              <input id="uploader" type="file" onChange={this.upload} />
            </div>
          </label>
          <div className="me_item me_nameArea">
            <span>昵称</span>
            <span className="me_name">{userName}</span>
          </div>
        </div>
        <Toast />
      </React.Fragment>
    );
  }

  upload = e => {
    const file = Array.from(e.target.files)[0];

    const types = ["image/png", "image/jpeg"];

    if (types.every(type => file.type !== type)) {
      showToast("只可选择png或者jpeg");
      return;
    }

    if (file.size > 150000) {
      showToast("所选图片大小超过限制");
      return;
    }

    const formData = new FormData();
    formData.append("pickedImg", file);

    fetch(`${varConfig.backendOrigin}/image-upload`, {
      method: "POST",
      body: formData,
      headers: {
        authorization: "Bearer" + this.props.token
      }
    })
      .then(res => {
        if (!res.ok) {
          throw res;
        }
        return res.json();
      })
      .then(user => {
        this.props.saveMyInfo(user);
      })
      .catch(err => {
        showToast(err);
      });
  };
}

function mapStateToProps(state) {
  const { token } = state.authState;
  const { userName, avatar } = state.myInfo;
  return {
    token,
    userName,
    avatar
  };
}

export default connect(
  mapStateToProps,
  { saveMyInfo }
)(Me);
