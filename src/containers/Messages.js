import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { fetchMessages, saveCurrentChatDetail } from "../actions/messages";

import Room from "../components/Room";

import { dbDateToRoomDate } from "../utils/date";

class Messages extends Component {
  render() {
    const rooms = this.props.messages.map(message => {
      const { messageFrom, messages, updated_at, id } = message;
      const chatWith = this.props.friends[messageFrom];
      const currentRoomLastMessage = messages[messages.length - 1].message_body;
      return (
        <Room
          enterRoom={() => this.navigateToChatDetail(id, chatWith, messages)}
          key={id}
          chatWith={chatWith}
          updatedAt={dbDateToRoomDate(updated_at)}
          lastMessage={currentRoomLastMessage}
        />
      );
    });
    return <React.Fragment>{rooms}</React.Fragment>;
  }

  navigateToChatDetail = (roomId, chatWith, messages) => {
    this.props.saveCurrentChatDetail({
      chatWith,
      messages
    });
    this.props.history.push(`/private/chatDetail/${roomId}`);
    //this.props.history.push(`/login`);
  };

  componentDidMount() {}
}

function mapStateToProps(state) {
  const { messages, friends } = state;
  return {
    messages,
    friends
  };
}

export default connect(
  mapStateToProps,
  { fetchMessages, saveCurrentChatDetail }
)(Messages);
