import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";

import { assentFriendRequset } from "../actions/friends";

import "./Friends.css";

class Friends extends Component {
  render() {
    const { friends, makeFriendsRequests } = this.props;
    const friendsArr = Object.entries(friends);
    const friendsComponent = friendsArr.map(([userId, rest]) => (
      <div
        key={userId}
        className="Friends_friend"
        onClick={() => this.navigateToUserProfile(userId)}
      >
        <img className="Friends_friend_avatar" src={rest.avatar} />
        <span>{rest.userName}</span>
      </div>
    ));

    const friendRequestsComponent = makeFriendsRequests.map(
      (request, index) => {
        const info = Object.values(request)[0];
        const { from: userId, userName, avatar, message } = info;
        return (
          <div key={userId} className="Friends_friendRequest">
            <div style={{ display: "flex", alignItems: "center" }}>
              <img className="Friends_friend_avatar" src={avatar} />
              <div>
                <div style={{ marginBottom: "5px" }}>{userName}</div>
                <div style={{ color: "rgba(190, 189, 193, 1)" }}>{message}</div>
              </div>
            </div>
            <button
              className="acceptButton"
              onClick={() => this.props.assentFriendRequset(userId)}
            >
              接受
            </button>
          </div>
        );
      }
    );
    return (
      <React.Fragment>
        <header className="Friends_header">
          <FontAwesomeIcon
            onClick={() => this.props.history.push(`/private/searchUser`)}
            icon="user-plus"
            size="2x"
            style={{ marginRight: "5px" }}
          />
        </header>
        <main className="Friends_main">
          {friendRequestsComponent}
          {friendsComponent}
        </main>
      </React.Fragment>
    );
  }

  navigateToUserProfile = userId => {
    this.props.history.push(`/private/userProfile/${userId}`);
  };
}

function mapStateToProps(state) {
  const { friends, makeFriendsRequests } = state;
  return {
    friends,
    makeFriendsRequests
  };
}

export default connect(
  mapStateToProps,
  { assentFriendRequset }
)(Friends);
