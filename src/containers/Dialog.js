import React, { Component } from "react";

import "./Dialog.css";

class Dialog extends Component {
  render() {
    const { title, confirm, errorMessage } = this.props;
    return (
      <div className="Dialog_wrapper">
        <article className="Dialog_content">
          <span className="Dialog_title">{title}</span>
          <p className="Dialog_errorMessage">{errorMessage}</p>
          <button className="Dialog_confirm" onClick={confirm}>
            确定
          </button>
        </article>
      </div>
    );
  }
}

export default Dialog;
