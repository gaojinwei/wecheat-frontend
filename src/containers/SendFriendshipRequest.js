import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";

import { saveSearchedUser } from "../actions/friends";

import GeneralHead from "../components/GeneralHead";
import GeneralMain from "../components/GeneralMain";

import varConfig from "../varConfig";

import "./SearchUser.css";

class SearchUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      typing: `我是${props.myName}`
    };
  }

  render() {
    return (
      <React.Fragment>
        <GeneralHead title="朋友验证" />
        <GeneralMain>
          <div
            style={{
              color: "rgba(190, 189, 193, 1)",
              width: "100%",
              paddingLeft: "15px",
              marginBottom: "10px",
              boxSizing: "border-box"
            }}
          >
            你需要发送验证申请，等对方通过
          </div>
          <form className="SearchUser_form" onSubmit={this.friendRequest}>
            <input
              className="SearchUser_input"
              onChange={this.typing}
              value={this.state.typing}
            />
            <button className="SearchUser_button">发送</button>
          </form>
        </GeneralMain>
      </React.Fragment>
    );
  }

  typing = e => {
    const typing = e.target.value;
    this.setState({
      typing
    });
  };

  friendRequest = e => {
    e.preventDefault();
    const { token, myId, makeFriendsWith, userDetailInfo } = this.props;
    const { typing } = this.state;
    window.identifySocket.emit("addFriendRequest", {
      from: myId,
      to: makeFriendsWith,
      message: typing,
      userDetailInfo
    });
    this.props.history.goBack();
  };
}

function mapStateToProps(state) {
  const { token, id } = state.authState;
  const { userName } = state.myInfo;
  const makeFriendsWith = state.searchedUser._id;
  return {
    token,
    myId: id,
    myName: userName,
    makeFriendsWith,
    userDetailInfo: state.myInfo
  };
}

export default connect(
  mapStateToProps,
  { saveSearchedUser }
)(SearchUser);
