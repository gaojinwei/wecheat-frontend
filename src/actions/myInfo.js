import { SAVE_MY_INFO } from "./constants/action-types";

export const saveMyInfo = myInfo => ({
  type: SAVE_MY_INFO,
  payload: myInfo
});
