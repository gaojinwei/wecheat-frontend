import {
  SET_USERNAME_VALUE,
  EMPTY_USERNAME_VALUE,
  SET_PASSWORD_VALUE,
  EMPTY_PASSWORD_VALUE,
  SET_SIGNUP_USERNAME,
  EMPTY_SIGNUP_USERNAME,
  SET_SIGNUP_PASSWORD,
  EMPTY_SIGNUP_PASSWORD,
  SET_SIGNUP_REPEAT_PASSWORD,
  EMPTY_SIGNUP_REPEAT_PASSWORD,
  SET_AUTH_ERROR,
  EMPTY_AUTH_ERROR,
  SET_AUTH_STATUS,
  SET_JWT_TOKEN,
  SAVE_USER_ID
} from "./constants/action-types";

export const setUsernameValue = value => ({
  type: SET_USERNAME_VALUE,
  payload: value
});

export const emptyUsernameValue = () => ({
  type: EMPTY_USERNAME_VALUE
});

export const setPasswordValue = value => ({
  type: SET_PASSWORD_VALUE,
  payload: value
});

export const emptyPasswordValue = () => ({
  type: EMPTY_PASSWORD_VALUE
});

//signup
export const setSignupUsername = value => ({
  type: SET_SIGNUP_USERNAME,
  payload: value
});

export const emptySignupUsername = () => ({
  type: EMPTY_SIGNUP_USERNAME
});

export const setSignupPassword = value => ({
  type: SET_SIGNUP_PASSWORD,
  payload: value
});

export const emptySignupPassword = () => ({
  type: EMPTY_SIGNUP_PASSWORD
});

export const setSignupRepeatPassword = value => ({
  type: SET_SIGNUP_REPEAT_PASSWORD,
  payload: value
});

export const emptySignupRepeatPassword = () => ({
  type: EMPTY_SIGNUP_REPEAT_PASSWORD
});

export const setAuthError = value => ({
  type: SET_AUTH_ERROR,
  payload: value
});

export const emptyAuthError = () => ({
  type: EMPTY_AUTH_ERROR
});

export const setAuthStatus = status => ({
  type: SET_AUTH_STATUS,
  payload: status
});

export const setJwtToken = token => ({
  type: SET_JWT_TOKEN,
  payload: token
});

export const saveUserId = id => ({
  type: SAVE_USER_ID,
  payload: id
});
