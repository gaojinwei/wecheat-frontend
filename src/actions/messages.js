import axios from "axios";
import io from "socket.io-client";

import {
  SAVE_MESSAGES,
  SAVE_CURRENT_CHAT_DETAIL,
  SET_MESSAGE_TYPING
} from "./constants/action-types";
import varConfig from "../varConfig";

export const fetchMessages = () => {
  return (dispatch, getState) => {
    const { authState } = getState();
    const { token, id } = authState;
    axios
      .get(`${varConfig.backendOrigin}/api/room/${id}`, {
        headers: {
          authorization: "Bearer" + token
        }
      })
      .then(res => {
        const data = res.data;
        // {
        //   messageFrom: $userId,
        //   messages: [{created_at, message_body,is_my_msg }]
        //   id: "",
        //   updated_at
        // }
        window.socketsCollection = {};
        const roomsHandled = data.rooms.map((item, index) => {
          const { messages, users, updated_at, _id, topic } = item;

          const currentRoomSocket = io.connect(
            `${varConfig.backendOrigin}/?roomId=${_id}`
          );
          window.socketsCollection[_id] = currentRoomSocket;
          currentRoomSocket.on("chat", function() {
            dispatch(fetchMessages());
            dispatch(fetchCurrentChatDetail(_id));
          });

          const room = {};
          room.updated_at = updated_at;
          room.id = _id;
          const usersExceptMe = users.filter((item, index) => {
            const { _id, username } = item;
            return _id !== id;
          });
          if (usersExceptMe.length === 1) {
            room.messageFrom = usersExceptMe[0]._id;
          } else {
            //只有多人聊天室有topic
            room.topic = topic;
          }

          const messagesHandled = messages.map((message, index) => {
            const messageStructure = {};
            const { created_at, message_body, message_status, user } = message;
            messageStructure.created_at = created_at;
            messageStructure.message_body = message_body;
            if (user === id) {
              messageStructure.is_my_msg = true;
            } else {
              messageStructure.is_my_msg = false;
            }
            return messageStructure;
          });
          room.messages = messagesHandled;
          return room;
        });
        //dispatch(saveMessages(data.rooms));
        dispatch(saveMessages(roomsHandled));
      });
  };
};

export const saveMessages = messages => ({
  type: SAVE_MESSAGES,
  payload: messages
});

export const saveCurrentChatDetail = detail => ({
  type: SAVE_CURRENT_CHAT_DETAIL,
  payload: detail
});

export const fetchCurrentChatDetail = roomId => {
  return (dispatch, getState) => {
    const { authState } = getState();
    const { token, id } = authState;
    axios
      .get(`${varConfig.backendOrigin}/api/room/chatDetail/${roomId}`, {
        headers: {
          authorization: "Bearer" + token
        }
      })
      .then(res => {
        const data = res.data;

        const { messages, users, updated_at, _id, topic } = data;

        const usersExceptMe = users.filter((item, index) => {
          const { _id, username } = item;
          return _id !== id;
        });
        var chatWith;
        if (usersExceptMe.length === 1) {
          chatWith = usersExceptMe[0];
        }

        const messagesHandled = messages.map((message, index) => {
          const messageStructure = {};
          const {
            created_at,
            message_body,
            message_status,
            user,
            _id
          } = message;
          messageStructure.id = _id;
          messageStructure.created_at = created_at;
          messageStructure.message_body = message_body;
          if (user === id) {
            messageStructure.is_my_msg = true;
          } else {
            messageStructure.is_my_msg = false;
          }
          return messageStructure;
        });

        dispatch(
          saveCurrentChatDetail({ chatWith, messages: messagesHandled })
        );
      });
  };
};

export const setMessageTyping = message => ({
  type: SET_MESSAGE_TYPING,
  payload: message
});

export const postMessage = (roomId, messageBody) => {
  return (dispatch, getState) => {
    const { authState } = getState();
    const { token, id } = authState;
    axios
      .post(
        `${varConfig.backendOrigin}/api/message/${roomId}/newMessage`,
        {
          user: id,
          message_body: messageBody
        },
        {
          headers: {
            authorization: "Bearer" + token
          }
        }
      )
      .then(res => {
        const data = res.data;
        if (data.success) {
          const currentRoomSocket = window.socketsCollection[roomId];
          currentRoomSocket.emit("chat");
          dispatch(setMessageTyping(""));
        }
      });
  };
};
