import axios from "axios";
import {
  FETCH_FRIENDS,
  SAVE_FRIENDS,
  SAVE_MY_INFO,
  SAVE_SEARCHED_USER,
  APPEND_FRIEND_REQUESTS,
  ASSENT_FRIEND_REQUEST,
  DELETE_HANDLED_REQUEST
} from "./constants/action-types";
import varConfig from "../varConfig";
import { fetchMessages } from "./messages";
import { saveMyInfo } from "./myInfo";

export const fetchFriends = () => {
  return (dispatch, getState) => {
    const { authState } = getState();
    const { token, id } = authState;
    axios
      .get(`${varConfig.backendOrigin}/api/user/${id}/friends`, {
        headers: {
          authorization: "Bearer" + token
        }
      })
      .then(res => {
        const data = res.data;
        dispatch(saveFriends(data.friends));
        dispatch(saveMyInfo(data.user));
        dispatch(fetchMessages());
      });
  };
};

export const saveFriends = friends => ({
  type: SAVE_FRIENDS,
  payload: friends
});

export const saveSearchedUser = userInfo => ({
  type: SAVE_SEARCHED_USER,
  payload: userInfo
});

export const appendFriendRequest = requestInfo => ({
  type: APPEND_FRIEND_REQUESTS,
  payload: requestInfo
});

export const assentFriendRequset = from => {
  return (dispatch, getState) => {
    const { authState, makeFriendsRequests } = getState();
    const { token, id } = authState;
    const currentRequest = makeFriendsRequests.filter(
      request => request[from].from === from
    )[0];
    const { from: whoseRequest } = Object.values(currentRequest)[0];
    axios
      .post(
        `${varConfig.backendOrigin}/api/user/makeNewFriend`,
        { whoseRequest },
        {
          headers: {
            authorization: "Bearer" + token
          }
        }
      )
      .then(res => {
        dispatch(deleteHandledRequest(whoseRequest));
        //接下来跳profile？
        window.identifySocket.emit("newFriendNotify", whoseRequest);
      });
  };
};

export const deleteHandledRequest = whoseRequest => ({
  type: DELETE_HANDLED_REQUEST,
  payload: whoseRequest
});
