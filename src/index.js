import React from "react";
import ReactDOM from "react-dom";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faUser,
  faUserFriends,
  faComment,
  faCompass,
  faPoo,
  faPlusCircle,
  faUserPlus,
  faTrashAlt,
  faAngleLeft
} from "@fortawesome/free-solid-svg-icons";
import "normalize.css";
import { Provider } from "react-redux";
import { store } from "./store";

import App from "./App.js";

library.add(
  faUser,
  faUserFriends,
  faComment,
  faCompass,
  faPoo,
  faPlusCircle,
  faUserPlus,
  faTrashAlt,
  faAngleLeft
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
