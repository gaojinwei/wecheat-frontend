const env = process.env.NODE_ENV || "development";

let envConfig = {};

switch (env) {
  case "development":
    envConfig = {
      //backendOrigin: "http://localhost:3001"
      backendOrigin: "http://localhost:3001"
    };
    break;
  case "production":
    envConfig = {
      //backendOrigin: "https://18.224.215.63:3001"
      backendOrigin: "https://gaojinwei.space:3002"
    };
    break;
  default:
}

export default envConfig;
