export function dbDateToRoomDate(dbDateStr) {
  const date = new Date(dbDateStr);
  if (isToday(date)) {
    return getHourMinute(date);
  } else if (isYesterday(date)) {
    return "昨天";
  } else {
    return getMouthDay(date);
  }
}

function isSameDay(d1, d2) {
  return (
    d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate()
  );
}

function isToday(d) {
  return isSameDay(d, new Date());
}

function isYesterday(d) {
  const yesterday = new Date();
  //遇到上一天是上一个月会自动切换月份
  yesterday.setDate(yesterday.getDate() - 1);
  return isSameDay(d, yesterday);
}

//09:10
function getHourMinute(d) {
  const hour = d.getHours();
  const minute = d.getMinutes();
  const hourStr = hour > 9 ? hour + "" : `0${hour}`;
  const minuteStr = minute > 9 ? minute + "" : `0${minute}`;
  return hourStr + ":" + minuteStr;
}

//6月1日
function getMouthDay(d) {
  const month = d.getMonth() + 1;
  const day = d.getDate();
  return month + "月" + day + "日";
}
