import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducer from "../reducers";

// const initialState = { tech: "React " };
// export const store = createStore(reducer, initialState);

export const store = createStore(reducer, {}, applyMiddleware(thunk));
